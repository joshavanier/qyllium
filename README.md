![Screenshot](screenshot.png)

**Qyllium** is a simple note-taking app for Linux, macOS, and Windows. Download it [here](https://joshavanier.itch.io/qyllium).

#### Development
```
npm install
npm start
```

---

**[Josh Avanier](https://joshavanier.github.io)**

**[Merveilles](https://merveilles.town/@joshavanier)** &middot; **[Twitter](https://twitter.com/joshavanier)**

**[MIT](LICENSE)**
