const {app, BrowserWindow, webFrame} = require('electron');

if (process.argv[2] && process.argv[2] === 'dev') require('electron-reload')(__dirname);

let win;

app.on('ready', function() {
  win = new BrowserWindow({
    width: 300,
    height: 500,
    backgroundColor: '#0c0c0c',
    resizable: false,
    autoHideMenuBar: true,
    frame: false,
    show: false,
    icon: `${__dirname}/icon.ico`
  });

  win.loadURL(`file://${__dirname}/src/index.html`);

  win.webContents.on('did-finish-load', function () {
    win.show()
    win.focus()
  });

  win.on('closed', () => {
    win = null
  });
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit();
});

app.on('activate', () => {
  if (win === null) createWindow();
});
